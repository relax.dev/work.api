import * as crypto from 'crypto';

export class RelaxCrypto {
  secretKey: string;

  constructor(secretKey: string) {
    this.secretKey = secretKey;
  }

  encrypt(payload: Record<string, string>) {
    const head = Buffer.from(
      JSON.stringify({
        alg: 'HS256',
        typ: 'jwt',
      }),
    ).toString('base64');

    const body = Buffer.from(JSON.stringify(payload)).toString('base64');
    const signature = crypto
      .createHmac('SHA256', this.secretKey)
      .update(`${head}.${body}`)
      .digest('base64');

    const token = `${head}.${body}.${signature}`;

    return token;
  }

  decrypt(hash: string) {
    const tokenParts = hash.split('.');

    const signature = crypto
      .createHmac('SHA256', this.secretKey)
      .update(`${tokenParts[0]}.${tokenParts[1]}`)
      .digest('base64');

    if (signature === tokenParts[2]) {
      const payload = Buffer.from(tokenParts[1], 'base64').toString('utf8');

      return JSON.parse(payload);
    } else {
      return null;
    }
  }
}
