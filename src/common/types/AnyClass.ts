export declare type AnyClass<ReturnType = any> = new (
  ...args: any[]
) => ReturnType;
