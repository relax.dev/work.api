import {
  Injectable,
  CanActivate,
  ExecutionContext,
  UnauthorizedException,
} from '@nestjs/common';
import { User } from '@relax/modules/users/entities/user.entity';

@Injectable()
export class EmailConfirmationGuard implements CanActivate {
  canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();

    if (!(request.user as User)?.isVerifiedEmail) {
      throw new UnauthorizedException('Confirm your email first');
    }

    return true;
  }
}
