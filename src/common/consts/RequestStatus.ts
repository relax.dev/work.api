export enum RequestStatus {
  IDLE = 0,
  IN_QUEUE = 1,
  SENDED_SUCCESS = 2,
  SENDED_ERROR = 3,
}
