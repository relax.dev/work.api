import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { UserDto } from '@relax/modules/users/dto/user.dto';
import { User } from '@relax/modules/users/entities/user.entity';

export const CurrentUser = createParamDecorator(
  (data: string, ctx: ExecutionContext): UserDto | null => {
    const user: User | undefined = ctx.switchToHttp().getRequest().user;

    if (!user) {
      return null;
    }

    return data ? user[data] : user;
  },
);
