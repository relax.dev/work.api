import { RolesBuilder } from 'nest-access-control';
import { AppResources } from '@relax/common/roles/app.resources';

export enum AppRoles {
  PARTICIPANT = 'participant',
  ADMIN = 'admin',
}

export const roles = new RolesBuilder();

roles
  .grant(AppRoles.PARTICIPANT)
  .readOwn([AppResources.USERS])

  .grant(AppRoles.ADMIN)
  .readAny([AppResources.USERS])
  .deleteAny([AppResources.USERS])
  .createAny([AppResources.USERS])
  .updateAny([AppResources.USERS]);
