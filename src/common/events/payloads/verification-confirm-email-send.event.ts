import { User } from '@relax/modules/users/entities/user.entity';

export class VerificationConfirmEmailSendEvent {
  user: User;
}
