import { IsNotEmpty } from 'class-validator';

export class VerificationConfirmEmailEvent {
  @IsNotEmpty()
  userId: string;
}
