export enum CommonEventType {
  VerificationsConfirmEmail = 'verifications.confirmEmail',
  VerificationsConfirmEmailSend = 'verifications.confirmEmailSend',
}
