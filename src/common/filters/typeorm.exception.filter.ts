import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
} from '@nestjs/common';
import { TypeORMError } from 'typeorm';

@Catch(TypeORMError)
export class TypeOrmFilter implements ExceptionFilter {
  catch(exception: TypeORMError, host: ArgumentsHost) {
    const response = host.switchToHttp().getResponse();
    let message: string = (exception as TypeORMError).message;
    const code: string = (exception as any).code;

    switch (code) {
      case '23505': {
        message = 'Dublicate key';
      }
    }

    const customResponse: HttpException = new HttpException(
      {
        timestamp: new Date().toISOString(),
        message,
        errorCode: code,
        statusCode: 500,
      },
      500,
    );

    response
      .status(customResponse.getStatus())
      .json(customResponse.getResponse());
  }
}
