import { Response as IResponse } from 'express';
import { ConfigService } from '@nestjs/config';
import { stringifyQuery } from 'nestjs-url-generator/dist/helpers';

export const makeRedirectResponse = (
  response: IResponse,
  configService: ConfigService,
  options: {
    configPath: string;
    query?: Record<string, string>;
    error?: Error;
  },
) => {
  const success = configService.get(
    `redirects.${options.configPath}.successUrl`,
  );
  const failure = configService.get(
    `redirects.${options.configPath}.failureUrl`,
  );

  if (options.error) {
    return response.redirect(
      failure + '?' + stringifyQuery({ errorMessage: options.error.message }),
    );
  }

  return response.redirect(success + '?' + stringifyQuery(options.query));
};
