export const millisecondsToSeconds = (milliseconds: number) => {
  return Math.floor(milliseconds / 1000);
};

export const secondsToMilliseconds = (seconds: number) => {
  return seconds * 1000;
};
