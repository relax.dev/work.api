import { Body, Controller, Post } from '@nestjs/common';
import { TokensService } from '@relax/modules/tokens/tokens.service';
import { TokenRefreshDto } from '@relax/modules/tokens/dto/token.refresh.dto';
import { ApiTags } from '@nestjs/swagger';

@Controller('tokens')
@ApiTags('tokens')
export class TokensController {
  constructor(private readonly tokensService: TokensService) {}

  @Post('refresh')
  refresh(@Body() payload: TokenRefreshDto) {
    return this.tokensService.refreshToken(payload.refreshToken);
  }
}
