import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ApplicationConfig } from '@relax/configuration/config';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RefreshToken } from '@relax/modules/tokens/entities/refresh-token.entity';
import { TokenCreateDto } from '@relax/modules/tokens/dto/token.create.dto';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '@relax/modules/users/users.service';
import { User } from '@relax/modules/users/entities/user.entity';
import { IJWTPayload } from '@relax/modules/tokens/interfaces/JWTPayload.types';

@Injectable()
export class TokensService {
  constructor(
    private readonly configService: ConfigService<ApplicationConfig>,
    private readonly jwtService: JwtService,
    private readonly usersService: UsersService,
    @InjectRepository(RefreshToken)
    private readonly refreshTokensRepository: Repository<RefreshToken>,
  ) {}

  async saveToken(payload: TokenCreateDto) {
    const existsRecord = await this.refreshTokensRepository.findOne({
      userId: payload.userId,
    });

    if (existsRecord) {
      return await this.refreshTokensRepository.update(existsRecord.id, {
        value: payload.refreshToken,
      });
    }

    const data = this.refreshTokensRepository.create({
      userId: payload.userId,
      value: payload.refreshToken,
    });

    return this.refreshTokensRepository.save(data);
  }

  async removeToken(userId: string) {
    const existsRecord = await this.refreshTokensRepository.findOne({
      userId: userId,
    });

    if (existsRecord) {
      return await this.refreshTokensRepository.delete(existsRecord.id);
    }

    return true;
  }

  async generateTokens(payload: User) {
    const data: IJWTPayload = {
      id: payload.id,
    };

    return {
      accessToken: this.jwtService.sign(data, {
        expiresIn: this.configService.get('tokens.access.expiresIn', {
          infer: true,
        }),
        secret: this.configService.get('tokens.access.secretKey', {
          infer: true,
        }),
      }),
      refreshToken: this.jwtService.sign(data, {
        expiresIn: this.configService.get('tokens.refresh.expiresIn', {
          infer: true,
        }),
        secret: this.configService.get('tokens.refresh.secretKey', {
          infer: true,
        }),
      }),
    };
  }

  validateRefreshToken(token: string): IJWTPayload {
    try {
      return this.jwtService.verify(token, {
        secret: this.configService.get('tokens.refresh.secretKey', {
          infer: true,
        }),
      });
    } catch (e) {
      throw new UnauthorizedException('Malformed refresh token');
    }
  }

  async refreshToken(token: string) {
    if (!token) {
      throw new UnauthorizedException('Empty refresh token');
    }

    const payload = this.validateRefreshToken(token);

    const existedToken = await this.refreshTokensRepository.findOne({
      value: token,
      userId: payload.id,
    });

    if (!existedToken) {
      throw new UnauthorizedException('Non existent refresh token');
    }

    const user = await this.usersService.findById(payload.id);
    const tokens = await this.generateTokens(user);

    await this.saveToken({
      userId: payload.id,
      refreshToken: tokens.refreshToken,
    });

    return tokens;
  }
}
