import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class TokenRefreshDto {
  @IsNotEmpty()
  @ApiProperty()
  refreshToken: string;
}
