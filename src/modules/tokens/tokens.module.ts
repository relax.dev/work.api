import { Module } from '@nestjs/common';
import { TokensService } from './tokens.service';
import { TokensController } from './tokens.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RefreshToken } from '@relax/modules/tokens/entities/refresh-token.entity';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ApplicationConfig } from '@relax/configuration/config';
import { UsersModule } from '@relax/modules/users/users.module';

@Module({
  imports: [
    UsersModule,
    TypeOrmModule.forFeature([RefreshToken]),
    JwtModule.registerAsync({
      useFactory: (config: ConfigService<ApplicationConfig>) => ({
        secret: config.get('tokens.access.secretKey', { infer: true }),
        signOptions: {
          expiresIn: config.get('tokens.access.expiresIn', { infer: true }),
        },
      }),
      imports: [ConfigModule],
      inject: [ConfigService],
    }),
  ],
  providers: [TokensService],
  controllers: [TokensController],
  exports: [TokensService],
})
export class TokensModule {}
