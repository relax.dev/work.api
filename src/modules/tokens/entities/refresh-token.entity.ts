import { BaseEntity } from '@relax/common/entities/BaseEntity';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class RefreshToken extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', unique: true })
  value: string;

  @Column({ type: 'varchar' })
  userId: string;
}
