import { HttpException, HttpStatus } from '@nestjs/common';

export const UserNotFoundException = new HttpException(
  'User not found',
  HttpStatus.BAD_REQUEST,
);
