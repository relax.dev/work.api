import {
  Column,
  Entity,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import { AppRoles } from '@relax/common/roles/app.roles';
import { Image } from '@relax/modules/images/entities/image.entity';
import { Profile } from '@relax/modules/profiles/entities/profile.entity';
import { BaseEntity } from '@relax/common/entities/BaseEntity';

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar', unique: true })
  email: string;

  @Column({ type: 'varchar' })
  @Exclude({ toPlainOnly: true })
  password: string;

  @Column({ type: 'enum', enum: AppRoles, default: AppRoles.PARTICIPANT })
  role: AppRoles;

  @Column({ type: 'boolean', default: false })
  isVerifiedEmail: boolean;

  @OneToMany(() => Image, (image) => image.user, { cascade: true })
  images: Image[];

  @OneToOne(() => Profile, (profile) => profile.user, {
    cascade: true,
    eager: true,
  })
  profile: Profile;
}
