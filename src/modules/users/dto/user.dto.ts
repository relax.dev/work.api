import { AppRoles } from '@relax/common/roles/app.roles';
import { ApiProperty } from '@nestjs/swagger';

export class UserDto {
  @ApiProperty()
  id: string;
  @ApiProperty()
  email: string;
  @ApiProperty({ enum: AppRoles })
  role: AppRoles;
  @ApiProperty()
  isVerifiedEmail: boolean;
}
