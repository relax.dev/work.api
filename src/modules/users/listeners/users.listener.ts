import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { UsersService } from '@relax/modules/users/users.service';
import { CommonEventType } from '@relax/common/events/types/common';
import { VerificationConfirmEmailEvent } from '@relax/common/events/payloads/verification-confirm-email.event';

@Injectable()
export class UsersListener {
  constructor(private readonly usersService: UsersService) {}
  @OnEvent(CommonEventType.VerificationsConfirmEmail)
  handleOrderCreatedEvent(event: VerificationConfirmEmailEvent) {
    this.usersService.markAsVerifiedEmail(event.userId).catch(() => {
      console.log('Can not mark as verified user email');
    });
  }
}
