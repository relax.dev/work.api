import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '@relax/modules/users/entities/user.entity';
import { UsersListener } from '@relax/modules/users/listeners/users.listener';
import { ProfilesModule } from '@relax/modules/profiles/profiles.module';

@Module({
  imports: [TypeOrmModule.forFeature([User]), ProfilesModule],
  controllers: [UsersController],
  providers: [UsersService, UsersListener],
  exports: [UsersService],
})
export class UsersModule {}
