import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { JwtAuthGuard } from '@relax/modules/auth/guards/jwt-auth.guard';
import { Paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { User } from '@relax/modules/users/entities/user.entity';
import { PaginateQueryOptions } from '@relax/common/decorators/PaginatedQueryOptions.decorator';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { ACGuard, UseRoles } from 'nest-access-control';
import { AppResources } from '@relax/common/roles/app.resources';
import { UserDto } from '@relax/modules/users/dto/user.dto';

@Controller('users')
@ApiTags('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  @Get()
  @PaginateQueryOptions()
  @UseRoles({
    resource: AppResources.USERS,
    action: 'read',
    possession: 'any',
  })
  @UseGuards(JwtAuthGuard, ACGuard)
  public findAll(@Paginate() query: PaginateQuery): Promise<Paginated<User>> {
    return this.usersService.findAll(query);
  }

  @Get(':id')
  @UseRoles({
    resource: AppResources.USERS,
    action: 'read',
    possession: 'any',
  })
  @UseGuards(JwtAuthGuard, ACGuard)
  @ApiResponse({ type: UserDto })
  findOne(@Param('id') id: string): Promise<UserDto> {
    return this.usersService.findById(id);
  }

  @Patch(':id')
  @UseRoles({
    resource: AppResources.USERS,
    action: 'update',
    possession: 'any',
  })
  @UseGuards(JwtAuthGuard, ACGuard)
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(id, updateUserDto);
  }

  @Delete(':id')
  @UseRoles({
    resource: AppResources.USERS,
    action: 'update',
    possession: 'any',
  })
  @UseGuards(JwtAuthGuard, ACGuard)
  remove(@Param('id') id: string) {
    return this.usersService.remove(id);
  }
}
