import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { ConfigService } from '@nestjs/config';
import { ApplicationConfig } from '@relax/configuration/config';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '@relax/modules/users/entities/user.entity';
import { In, Repository } from 'typeorm';
import { MailerService } from '@nestjs-modules/mailer';
import * as bcrypt from 'bcrypt';
import {
  FilterOperator,
  paginate,
  Paginated,
  PaginateQuery,
} from 'nestjs-paginate';
import { Profile } from '@relax/modules/profiles/entities/profile.entity';

@Injectable()
export class UsersService {
  constructor(
    private readonly mailerService: MailerService,
    private readonly configService: ConfigService<ApplicationConfig>,
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    const existedEmail = await this.findByEmail(createUserDto.email);

    if (existedEmail) {
      throw new HttpException('User already existed', HttpStatus.BAD_REQUEST);
    }

    const { password, email } = createUserDto;

    const profile = new Profile();

    const data = this.usersRepository.create({
      email,
      profile,
      password: password ? bcrypt.hashSync(password, bcrypt.genSaltSync()) : '',
    });

    return this.usersRepository.save(data);
  }

  async findAll(query: PaginateQuery): Promise<Paginated<User>> {
    return paginate(query, this.usersRepository, {
      sortableColumns: ['id', 'email'],
      searchableColumns: ['email'],
      defaultSortBy: [['id', 'DESC']],
      filterableColumns: {
        isVerifiedEmail: [FilterOperator.EQ],
        email: [FilterOperator.EQ],
      },
    });
  }

  findByIds(ids: string[]) {
    return this.usersRepository.find({ where: { id: In(ids) } });
  }

  findById(id: string) {
    return this.usersRepository.findOne({ id }, { relations: ['profile'] });
  }

  findByEmail(email: string) {
    return this.usersRepository.findOne({ email });
  }

  update(id: string, updateUserDto: UpdateUserDto) {
    return this.usersRepository.update(id, updateUserDto);
  }

  remove(id: string) {
    return this.usersRepository.softDelete(id);
  }

  async markAsVerifiedEmail(id: string) {
    const user = await this.findById(id);

    if (user.isVerifiedEmail) {
      throw new HttpException('Email already verified', HttpStatus.CONFLICT);
    }

    return this.usersRepository.update(id, {
      isVerifiedEmail: true,
    });
  }

  markAsUnverifiedEmail(id: string) {
    return this.usersRepository.update(id, {
      isVerifiedEmail: false,
    });
  }
}
