import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { randomUUID } from 'crypto';
import { InjectS3, S3 } from 'nestjs-s3';
import { Repository } from 'typeorm';
import { Image } from './entities/image.entity';
import { ConfigService } from '@nestjs/config';
import { ImageCreateDto } from '@relax/modules/images/dto/image-create.dto';
import { ImageSizeDto } from './dto/image-size.dto';
import * as sharp from 'sharp';

@Injectable()
export class ImagesService {
  constructor(
    @InjectS3()
    private s3: S3,
    @InjectRepository(Image)
    private imagesRepository: Repository<Image>,

    private configService: ConfigService,
  ) {}

  async upload(payload: ImageCreateDto) {
    const imageKey = randomUUID();

    const bucket = this.configService.get('s3.bucket', { infer: true });

    const sharped = await sharp(payload.file.buffer);
    const metadata = sharped.metadata();

    const sizes: ImageSizeDto[] = payload.sizes || [
      {
        width: metadata.width,
        height: metadata.height,
      },
    ];

    const responses = await Promise.all(
      sizes.map(async ({ width }) => {
        const resized = await sharp(payload.file.buffer).resize({
          width,
          fit: 'cover',
          position: 'center',
        });

        const buffer = await resized.toFormat('jpeg').toBuffer();
        const resizedMeta = await sharp(buffer).metadata();

        const response = await this.s3
          .upload({
            Key: `${imageKey}-${resizedMeta.width}x${resizedMeta.height}`,
            Body: buffer,
            Bucket: bucket,
          })
          .promise();

        return {
          url: response.Location,
          width: resizedMeta.width,
          height: resizedMeta.height,
        };
      }),
    );

    const data = this.imagesRepository.create({
      id: imageKey,
      sizes: responses,
      user: {
        id: payload.userId,
      },
    });

    await this.imagesRepository.save(data);

    return this.imagesRepository.findOne({ id: imageKey });
  }

  async remove(id: string) {
    const bucket = this.configService.get('s3.bucket', { infer: true });

    const image = await this.imagesRepository.findOne(
      { id },
      { relations: ['user'] },
    );

    if (!image) {
      throw new HttpException('Wrong image id', HttpStatus.BAD_REQUEST);
    }

    const sizes = image.sizes;

    await Promise.all(
      sizes.map(async ({ width, height }) => {
        return this.s3
          .deleteObject({
            Key: `${image.id}-${width}x${height}`,
            Bucket: bucket,
          })
          .promise();
      }),
    );

    return this.imagesRepository.delete(id);
  }
}
