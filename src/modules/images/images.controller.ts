import {
  Body,
  Controller,
  Delete,
  Param,
  Post,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ImagesService } from '@relax/modules/images/images.service';
import { JwtAuthGuard } from '@relax/modules/auth/guards/jwt-auth.guard';
import { CurrentUser } from '@relax/common/decorators/CurrentUser.decorator';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { ImageCreateDto } from '@relax/modules/images/dto/image-create.dto';
import { User } from '@relax/modules/users/entities/user.entity';
import { ImageSizeDto } from './dto/image-size.dto';

const sizesToNumber = (sizes?: ImageSizeDto[]) => {
  return sizes?.map(({ width, height }) => ({
    width: Number(width),
    height: Number(height),
  }));
};
@Controller('images')
@ApiTags('images')
export class ImagesController {
  constructor(private readonly imagesService: ImagesService) {}

  @Post('upload')
  @UseInterceptors(FileInterceptor('file'))
  @UseGuards(JwtAuthGuard)
  @ApiBody({ type: ImageCreateDto })
  async upload(
    @UploadedFile() file: Express.Multer.File,
    @CurrentUser() user: User,
    @Body() body: ImageCreateDto,
  ) {
    const { sizes } = body;
    return this.imagesService.upload({
      file,
      userId: user.id,
      sizes: sizesToNumber(sizes),
    });
  }

  @Delete('/:id')
  //Защитить
  @UseGuards(JwtAuthGuard)
  async remove(@Param('id') id: string) {
    return this.imagesService.remove(id);
  }
}
