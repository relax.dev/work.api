import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm';
import { User } from '@relax/modules/users/entities/user.entity';
import { ImageSizeDto } from '../dto/image-size.dto';
import { BaseEntity } from '@relax/common/entities/BaseEntity';

@Entity()
export class Image extends BaseEntity {
  @PrimaryColumn()
  id: string;

  @Column({
    type: 'jsonb',
    nullable: true,
    array: false,
    default: () => "'[]'",
  })
  sizes: ImageSizeDto[];

  @ManyToOne(() => User, (user) => user.images)
  user: User;
}
