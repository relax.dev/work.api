import { ApiProperty } from '@nestjs/swagger';
import { ImageSizeDto } from './image-size.dto';

export class ImageDto {
  @ApiProperty()
  id: string;

  @ApiProperty()
  sizes?: ImageSizeDto[];
}
