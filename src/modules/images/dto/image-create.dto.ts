import { ApiProperty } from '@nestjs/swagger';
import { ImageSizeDto } from './image-size.dto';

export class ImageCreateDto {
  @ApiProperty({ type: 'string', format: 'binary' })
  file: Express.Multer.File;
  sizes?: ImageSizeDto[];
  userId: string;
}
