import { ApiProperty } from '@nestjs/swagger';

export class ImageSizeDto {
  @ApiProperty()
  width?: number;
  @ApiProperty()
  height?: number;
  @ApiProperty()
  url?: string;
}
