import { Module } from '@nestjs/common';
import { ImagesController } from './images.controller';
import { S3Module } from 'nestjs-s3';
import { ImagesService } from './images.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Image } from '@relax/modules/images/entities/image.entity';

@Module({
  imports: [
    S3Module.forRoot({
      config: {
        accessKeyId: 'YCAJErxETK1ZBQEh4y2nb2Aw6',
        secretAccessKey: 'YCPAEPbVLzB3UB_lCutIReQHUrRc_CyAMjS6tph7',
        endpoint: 'storage.yandexcloud.net',
        s3ForcePathStyle: true,
        signatureVersion: 'v4',
      },
    }),
    TypeOrmModule.forFeature([Image]),
  ],
  controllers: [ImagesController],
  providers: [ImagesService],
  exports: [ImagesService],
})
export class ImagesModule {}
