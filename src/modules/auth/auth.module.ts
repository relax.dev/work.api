import { Module } from '@nestjs/common';
import { createConfig } from '@relax/configuration/config';
import { UsersModule } from '@relax/modules/users/users.module';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { LocalStrategy } from '@relax/modules/auth/strategies/local.strategy';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from '@relax/modules/auth/strategies/jwt.strategy';
import { TokensModule } from '@relax/modules/tokens/tokens.module';
import { GoogleStrategy } from '@relax/modules/auth/strategies/google.strategy';
import { VerificationsModule } from '@relax/modules/verifications/verifications.module';

createConfig();
@Module({
  imports: [UsersModule, VerificationsModule, PassportModule, TokensModule],
  providers: [AuthService, LocalStrategy, JwtStrategy, GoogleStrategy],
  controllers: [AuthController],
})
export class AuthModule {}
