import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ApplicationConfig } from '@relax/configuration/config';
import { UsersService } from '@relax/modules/users/users.service';
import { IJWTPayload } from '@relax/modules/tokens/interfaces/JWTPayload.types';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly configService: ConfigService<ApplicationConfig>,
    private readonly usersService: UsersService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get('tokens.access.secretKey', {
        infer: true,
      }),
    });
  }

  async validate(payload: IJWTPayload) {
    return await this.usersService.findById(payload.id);
  }
}
