import { PassportStrategy } from '@nestjs/passport';
import { Strategy, VerifyCallback } from 'passport-google-oauth20';
import { config } from 'dotenv';

import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ApplicationConfig } from '@relax/configuration/config';
import { UsersService } from '@relax/modules/users/users.service';
import { AuthService } from '@relax/modules/auth/auth.service';
import { CreateUserDto } from '@relax/modules/users/dto/create-user.dto';

config();

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {
  constructor(
    private readonly configService: ConfigService<ApplicationConfig>,
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
  ) {
    super({
      clientID: configService.get('google.clientId', { infer: true }),
      clientSecret: configService.get('google.clientSecret', { infer: true }),
      callbackURL: configService.get('google.redirectUrl', { infer: true }),
      scope: configService.get('google.scope', { infer: true }),
    });
  }

  async validate(
    accessToken: string,
    refreshToken: string,
    profile: any,
    done: VerifyCallback,
  ): Promise<void> {
    const { emails } = profile;

    const user: CreateUserDto = {
      email: emails[0].value,
    };

    // по умолчанию нужно задавать какой нибудь пароль и отправлять на почту
    const existedUser = await this.usersService.findByEmail(user.email);

    if (existedUser) {
      done(null, existedUser);
    } else {
      const newUser = await this.authService.register(user);
      done(null, newUser);
    }

    done(null, user);
  }
}
