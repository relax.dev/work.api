import { Injectable } from '@nestjs/common';
import { UsersService } from '@relax/modules/users/users.service';
import { TokensService } from '@relax/modules/tokens/tokens.service';
import * as bcrypt from 'bcrypt';
import { CreateUserDto } from '@relax/modules/users/dto/create-user.dto';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { VerificationConfirmEmailSendEvent } from '@relax/common/events/payloads/verification-confirm-email-send.event';
import { CommonEventType } from '@relax/common/events/types/common';
import { User } from '@relax/modules/users/entities/user.entity';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private tokensService: TokensService,
    private readonly eventEmitter: EventEmitter2,
  ) {}

  async validateUser(email: string, pass: string) {
    const user = await this.usersService.findByEmail(email);

    if (user && bcrypt.compareSync(pass, user.password)) {
      delete user.password;
      return user;
    }

    return null;
  }

  async register(payload: CreateUserDto) {
    try {
      const user = await this.usersService.create(payload);
      const event = new VerificationConfirmEmailSendEvent();
      event.user = user;

      this.eventEmitter.emit(
        CommonEventType.VerificationsConfirmEmailSend,
        event,
      );

      return user;
    } catch (e) {
      throw e;
    }
  }

  async login(user: User) {
    const tokens = await this.tokensService.generateTokens(user);

    await this.tokensService.saveToken({
      userId: user.id,
      refreshToken: tokens.refreshToken,
    });

    return tokens;
  }

  async logout(userId: string) {
    await this.tokensService.removeToken(userId);
  }
}
