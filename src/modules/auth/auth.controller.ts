import { Controller, Get, Post, UseGuards, Response } from '@nestjs/common';
import { LocalAuthGuard } from '@relax/modules/auth/guards/local-auth.guard';
import { CurrentUser } from '@relax/common/decorators/CurrentUser.decorator';
import { AuthService } from '@relax/modules/auth/auth.service';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '@relax/modules/auth/guards/jwt-auth.guard';
import { AuthLocalSignInDto } from '@relax/modules/auth/dto/auth-local.signin.dto';
import { AuthGuard } from '@nestjs/passport';
import { Response as IResponse } from 'express';
import { ConfigService } from '@nestjs/config';
import { ApplicationConfig } from '@relax/configuration/config';
import { makeRedirectResponse } from '@relax/common/helpers/makeRedirectResponse';
import { User } from '@relax/modules/users/entities/user.entity';

@Controller('auth')
@ApiTags('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly configService: ConfigService<ApplicationConfig>,
  ) {}

  @UseGuards(LocalAuthGuard)
  @Post('local/signin')
  @ApiBody({ type: AuthLocalSignInDto })
  localSignIn(@CurrentUser() user: User) {
    return this.authService.login(user);
  }

  @UseGuards(JwtAuthGuard)
  @Post('logout')
  logOut(@CurrentUser() user: User) {
    return this.authService.logout(user.id);
  }

  @Get('google')
  @UseGuards(AuthGuard('google'))
  async googleAuth() {
    return true;
  }

  @Get('google/redirect')
  @UseGuards(AuthGuard('google'))
  async googleAuthRedirect(
    @CurrentUser() user: User,
    @Response() response: IResponse,
  ) {
    try {
      const tokens = await this.authService.login(user);

      return makeRedirectResponse(response, this.configService, {
        configPath: 'authorization',
        query: tokens,
      });
    } catch (error) {
      return makeRedirectResponse(response, this.configService, {
        configPath: 'authorization',
        error,
      });
    }
  }
}
