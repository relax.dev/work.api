import { Test, TestingModule } from '@nestjs/testing';
import { DailyAwardsTablesService } from './daily-awards-tables.service';

describe('DailyAwardsTablesService', () => {
  let service: DailyAwardsTablesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DailyAwardsTablesService],
    }).compile();

    service = module.get<DailyAwardsTablesService>(DailyAwardsTablesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
