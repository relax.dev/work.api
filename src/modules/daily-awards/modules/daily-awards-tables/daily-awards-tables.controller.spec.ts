import { Test, TestingModule } from '@nestjs/testing';
import { DailyAwardsTablesController } from './daily-awards-tables.controller';
import { DailyAwardsTablesService } from './daily-awards-tables.service';

describe('DailyAwardsTablesController', () => {
  let controller: DailyAwardsTablesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DailyAwardsTablesController],
      providers: [DailyAwardsTablesService],
    }).compile();

    controller = module.get<DailyAwardsTablesController>(
      DailyAwardsTablesController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
