import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ProjectRoles } from '@relax/modules/projects/consts/roles';
import { ProjectRolesComplexGuard } from '@relax/modules/projects/guards/project-roles-complex.guard';
import { ProjectHook } from '@relax/modules/projects/hooks/project.hook';
import { PaginateQuery } from 'nestjs-paginate';
import { DailyAwardsTablesService } from './daily-awards-tables.service';
import { CreateDailyAwardsTableDto } from './dto/create-daily-awards-table.dto';
import { UpdateDailyAwardsTableDto } from './dto/update-daily-awards-table.dto';
import { ProjectFromTableHook } from './hooks/project-from-table.hook';

@Controller('daily-awards-tables')
@ApiTags('daily-awards-tables')
export class DailyAwardsTablesController {
  constructor(
    private readonly dailyAwardsTablesService: DailyAwardsTablesService,
  ) {}

  @Post()
  @ProjectRolesComplexGuard({
    roles: [ProjectRoles.ADMIN, ProjectRoles.EDITOR],
    hook: ProjectHook,
  })
  create(@Body() createDailyAwardsTableDto: CreateDailyAwardsTableDto) {
    return this.dailyAwardsTablesService.create({
      ...createDailyAwardsTableDto,
    });
  }

  @Get('project/:projectId')
  @ProjectRolesComplexGuard({
    roles: [ProjectRoles.ADMIN, ProjectRoles.EDITOR],
    hook: ProjectHook,
  })
  findByProject(
    @Query() query: PaginateQuery,
    @Param('projectId') projectId: string,
  ) {
    return this.dailyAwardsTablesService.findByProject(projectId, query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dailyAwardsTablesService.findOne(id);
  }

  @Patch(':id')
  @ProjectRolesComplexGuard({
    roles: [ProjectRoles.ADMIN, ProjectRoles.EDITOR],
    hook: ProjectFromTableHook,
  })
  update(
    @Param('id') id: string,
    @Body() updateDailyAwardsTableDto: UpdateDailyAwardsTableDto,
  ) {
    return this.dailyAwardsTablesService.update(id, updateDailyAwardsTableDto);
  }

  @Delete(':id')
  @ProjectRolesComplexGuard({
    roles: [ProjectRoles.ADMIN, ProjectRoles.EDITOR],
    hook: ProjectFromTableHook,
  })
  remove(@Param('id') id: string) {
    return this.dailyAwardsTablesService.remove(id);
  }
}
