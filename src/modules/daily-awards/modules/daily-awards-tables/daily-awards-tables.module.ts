import { Module } from '@nestjs/common';
import { DailyAwardsTablesService } from './daily-awards-tables.service';
import { DailyAwardsTablesController } from './daily-awards-tables.controller';
import { ProjectsModule } from '@relax/modules/projects/projects.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DailyAwardsTable } from './entities/daily-awards-table.entity';

@Module({
  imports: [ProjectsModule, TypeOrmModule.forFeature([DailyAwardsTable])],
  controllers: [DailyAwardsTablesController],
  providers: [DailyAwardsTablesService],
  exports: [DailyAwardsTablesService],
})
export class DailyAwardsTablesModule {}
