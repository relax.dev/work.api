import { ApiProperty } from '@nestjs/swagger';
import { ProjectDto } from '@relax/modules/projects/dto/project.dto';
import { IsNotEmpty } from 'class-validator';
import { DailyAwardsTableItemDto } from './daily-awards-table-item.dto';

export class DailyAwardsTableDto {
  @ApiProperty()
  project: ProjectDto;

  @IsNotEmpty()
  @ApiProperty({ type: [DailyAwardsTableItemDto] })
  data: DailyAwardsTableItemDto[];
}
