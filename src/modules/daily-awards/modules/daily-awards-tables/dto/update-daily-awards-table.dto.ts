import { PartialType } from '@nestjs/mapped-types';
import { CreateDailyAwardsTableDto } from './create-daily-awards-table.dto';

export class UpdateDailyAwardsTableDto extends PartialType(
  CreateDailyAwardsTableDto,
) {}
