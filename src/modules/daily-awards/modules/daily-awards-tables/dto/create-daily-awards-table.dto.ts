import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmpty, ValidateNested } from 'class-validator';
import { DailyAwardsTableItemDto } from './daily-awards-table-item.dto';

export class CreateDailyAwardsTableDto {
  @ApiProperty()
  @IsNotEmpty()
  projectId: string;

  @IsNotEmpty()
  @ValidateNested()
  @Type(() => DailyAwardsTableItemDto)
  @ApiProperty({ type: [DailyAwardsTableItemDto] })
  data: DailyAwardsTableItemDto[];
}
