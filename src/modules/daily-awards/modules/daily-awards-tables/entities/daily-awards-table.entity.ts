import { BaseEntity } from '@relax/common/entities/BaseEntity';
import { Project } from '@relax/modules/projects/entities/project.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { DailyAwardsReceiving } from '../../daily-awards-receiving/entities/daily-awards-receiving.entity';
import { DailyAwardsTableItemDto } from '../dto/daily-awards-table-item.dto';

@Entity()
export class DailyAwardsTable extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => Project, (project) => project.dailyAwardsTables)
  project: Project;

  @Column({ type: 'jsonb', default: () => "'[]'" })
  data: DailyAwardsTableItemDto[];

  @OneToMany(() => DailyAwardsReceiving, (receiving) => receiving.table, {
    cascade: true,
  })
  receivings: DailyAwardsReceiving[];
}
