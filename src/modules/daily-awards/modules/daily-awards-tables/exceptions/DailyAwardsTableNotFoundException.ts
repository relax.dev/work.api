import { HttpException, HttpStatus } from '@nestjs/common';

export const DailyAwardsTableNotFoundException = new HttpException(
  'Awards table not found',
  HttpStatus.BAD_REQUEST,
);
