import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { paginate, PaginateQuery } from 'nestjs-paginate';
import { Repository } from 'typeorm';
import { CreateDailyAwardsTableDto } from './dto/create-daily-awards-table.dto';
import { UpdateDailyAwardsTableDto } from './dto/update-daily-awards-table.dto';
import { DailyAwardsTable } from './entities/daily-awards-table.entity';

@Injectable()
export class DailyAwardsTablesService {
  constructor(
    @InjectRepository(DailyAwardsTable)
    private readonly tablesRepository: Repository<DailyAwardsTable>,
  ) {}
  create(createDailyAwardsTableDto: CreateDailyAwardsTableDto) {
    const data = this.tablesRepository.create({
      ...createDailyAwardsTableDto,
      project: {
        id: createDailyAwardsTableDto.projectId,
      },
    });

    return this.tablesRepository.save(data);
  }

  findByProject(projectId: string, query: PaginateQuery) {
    return paginate(query, this.tablesRepository, {
      relations: ['project'],
      where: {
        project: { id: projectId },
      },
      sortableColumns: ['project.id'],
    });
  }

  findOne(id: string) {
    return this.tablesRepository.findOne({ id }, { relations: ['project'] });
  }

  update(id: string, updateDailyAwardsTableDto: UpdateDailyAwardsTableDto) {
    return this.tablesRepository.update(id, updateDailyAwardsTableDto);
  }

  remove(id: string) {
    return this.tablesRepository.softDelete(id);
  }
}
