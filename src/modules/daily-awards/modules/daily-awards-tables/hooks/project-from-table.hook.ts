import { ExecutionContext, Injectable } from '@nestjs/common';
import { IProjectHook } from '@relax/modules/projects/decorators/RequiredProjectHook';
import { DailyAwardsTablesService } from '../daily-awards-tables.service';

@Injectable()
export class ProjectFromTableHook implements IProjectHook {
  constructor(private readonly tablesService: DailyAwardsTablesService) {}

  async getProjectId(ctx: ExecutionContext) {
    const request = ctx.switchToHttp().getRequest();
    const { id } = request.params;

    const table = await this.tablesService.findOne(id);

    if (!table) return null;

    return table.project.id ?? null;
  }
}
