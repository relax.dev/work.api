import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { ProjectUID } from '@relax/modules/projects/decorators/ProjectUID';
import { ProjectUIDGuard } from '@relax/modules/projects/guards/project-uid.guard';
import { DailyAwardsReceivingService } from './daily-awards-receiving.service';
import { CreateDailyAwardsReceivingDto } from './dto/create-daily-awards-receiving.dto';
import { DailyAwardCommonResponseDto } from './dto/daily-award-common-response.dto';

@Controller('daily-awards-receiving')
@ApiTags('daily-awards-receiving')
export class DailyAwardsReceivingController {
  constructor(
    private readonly dailyAwardsReceivingService: DailyAwardsReceivingService,
  ) {}

  @Post()
  @ApiResponse({ type: DailyAwardCommonResponseDto })
  @UseGuards(ProjectUIDGuard)
  create(
    @Body() dto: CreateDailyAwardsReceivingDto,
    @ProjectUID() uid: string,
  ) {
    return this.dailyAwardsReceivingService.create(dto, uid);
  }

  @Get('/table/:tableId')
  @ApiResponse({ type: DailyAwardCommonResponseDto })
  @UseGuards(ProjectUIDGuard)
  getTableForUID(@Param('tableId') tableId: string, @ProjectUID() uid: string) {
    return this.dailyAwardsReceivingService.getTableForUID(tableId, uid);
  }
}
