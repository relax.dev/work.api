import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { millisecondsToSeconds } from '@relax/common/helpers/dates';
import { Repository } from 'typeorm';
import { DailyAwardsTablesService } from '../daily-awards-tables/daily-awards-tables.service';
import { DailyAwardsTableItemDto } from '../daily-awards-tables/dto/daily-awards-table-item.dto';
import { CreateDailyAwardsReceivingDto } from './dto/create-daily-awards-receiving.dto';
import { DailyAwardsReceiving } from './entities/daily-awards-receiving.entity';
import { AwardAlreadyReceivedException } from './exceptions/AwardAlreadyReceivedException';
import { AwardDayIncorrectException } from './exceptions/AwardDayIncorrectException';
import { AwardNeedReceivePreviousException } from './exceptions/AwardNeedReceivePrevious';
import { AwardNeedWaitException } from './exceptions/AwardNeedWaitException';
import { DailyAwardCommonResponseDto } from './dto/daily-award-common-response.dto';

const getSecondsBetweenTwoTimes = (current: number, prev: number) => {
  return (current - prev) / 1000;
};

const MIN_TIME_TO_NEXT_AWARD = 5; // Ждем 5 секунд после получения награды
const MAX_TIME_TO_NEXT_AWARD = 60; // Если после получения награды прошло больше минуты - сбрасываем на 1 день.

const getNextRecieveDay = ({
  data,
  lastRecieveAt,
  lastRecieveDay,
  maxSecondsForNextAward,
}: {
  data: DailyAwardsTableItemDto[];
  lastRecieveAt: Date;
  lastRecieveDay: number;
  maxSecondsForNextAward: number;
}) => {
  const lastDay = data[data.length - 1].day;
  const lastRecieveIsEnd = lastRecieveDay >= lastDay;
  const currentSeconds = new Date().getTime();
  const prevRecieveSeconds = lastRecieveAt.getTime();

  const secondsBetweenDates = getSecondsBetweenTwoTimes(
    currentSeconds,
    prevRecieveSeconds,
  );

  if (lastRecieveIsEnd) {
    return data[0].day;
  }

  if (secondsBetweenDates > maxSecondsForNextAward) {
    return data[0].day;
  }

  return lastRecieveDay + 1;
};
@Injectable()
export class DailyAwardsReceivingService {
  constructor(
    @InjectRepository(DailyAwardsReceiving)
    private readonly receivingsRepository: Repository<DailyAwardsReceiving>,
    private readonly tablesService: DailyAwardsTablesService,
  ) {}

  async create(
    dto: CreateDailyAwardsReceivingDto,
    uid: string,
  ): Promise<DailyAwardCommonResponseDto> {
    const table = await this.tablesService.findOne(dto.tableId);

    const lastRecieve = await this.findLastRecieveForUID(uid);

    const lastDay = table.data[table.data.length - 1].day;
    const firstDay = table.data[0].day;

    if (dto.day > lastDay) {
      throw AwardDayIncorrectException;
    }

    if (dto.day < firstDay) {
      throw AwardDayIncorrectException;
    }

    if (lastRecieve) {
      const currentSeconds = new Date().getTime();
      const prevRecieveSeconds = lastRecieve.recievedAt.getTime();

      const secondsBetweenDates = getSecondsBetweenTwoTimes(
        currentSeconds,
        prevRecieveSeconds,
      );

      const minSecondsForNextAward = MIN_TIME_TO_NEXT_AWARD;
      const maxSecondsForNextAward = MAX_TIME_TO_NEXT_AWARD;

      const nextDay = getNextRecieveDay({
        data: table.data,
        lastRecieveAt: lastRecieve.recievedAt,
        lastRecieveDay: lastRecieve.day,
        maxSecondsForNextAward,
      });

      if (dto.day > nextDay) {
        throw AwardNeedReceivePreviousException;
      }

      if (dto.day < nextDay) {
        throw AwardAlreadyReceivedException;
      }

      if (secondsBetweenDates < minSecondsForNextAward) {
        throw AwardNeedWaitException;
      }

      if (dto.day === lastRecieve.day && lastRecieve.day !== nextDay) {
        throw AwardAlreadyReceivedException;
      }
    } else {
      if (dto.day !== firstDay) {
        throw AwardNeedReceivePreviousException;
      }
    }

    const data = this.receivingsRepository.create({
      uid,
      day: dto.day,
      table: {
        id: dto.tableId,
      },
    });

    await this.receivingsRepository.save(data);
    //Долбим бекенд который нас попросили

    // sendHttpPost(project.dailyRewardRecieved, { uid, sign, day });

    //Получение должно выглядеть так:

    //Если чел проебался на 2е суток - надо сбросить ему получение награды на 1 день.

    /* 

{
    table: [..13..],
    lastRecievedDay: 13,
    nextDay: 0,
}

{
    table: [..13..],
    lastRecievedDay: 12,
    nextDay: 13,
}
    */
    return this.getTableForUID(dto.tableId, uid);
  }

  async findLastRecieveForUID(uid: string) {
    const [data] = await this.receivingsRepository.find({
      where: { uid },
      order: { recievedAt: 'DESC' },
      take: 1,
    });

    return data;
  }

  async getTableForUID(
    tableId: string,
    uid: string,
  ): Promise<DailyAwardCommonResponseDto> {
    const table = await this.tablesService.findOne(tableId);

    const lastRecieve = await this.findLastRecieveForUID(uid);

    if (!lastRecieve) {
      return {
        nextDay: 1,
        lastRecieveDay: null,
        willResetAt: null,
        willAvaliableAt: null,
        data: table.data,
      };
    }

    const recievedAt = lastRecieve.recievedAt;
    const recievedDay = lastRecieve.day;

    const minSecondsForNextAward = MIN_TIME_TO_NEXT_AWARD;
    const maxSecondsForNextAward = MAX_TIME_TO_NEXT_AWARD;

    const nextDay = getNextRecieveDay({
      data: table.data,
      lastRecieveAt: recievedAt,
      lastRecieveDay: recievedDay,
      maxSecondsForNextAward,
    });

    return {
      nextDay,
      lastRecieveDay: recievedDay,
      willResetAt:
        millisecondsToSeconds(recievedAt.getTime()) + maxSecondsForNextAward,
      willAvaliableAt:
        millisecondsToSeconds(recievedAt.getTime()) + minSecondsForNextAward,
      data: table.data,
    };
  }
}
