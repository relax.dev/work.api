import { Module } from '@nestjs/common';
import { DailyAwardsReceivingService } from './daily-awards-receiving.service';
import { DailyAwardsReceivingController } from './daily-awards-receiving.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DailyAwardsReceiving } from './entities/daily-awards-receiving.entity';
import { DailyAwardsTablesModule } from '../daily-awards-tables/daily-awards-tables.module';
import { ProjectsModule } from '@relax/modules/projects/projects.module';

@Module({
  imports: [
    DailyAwardsTablesModule,
    ProjectsModule,
    TypeOrmModule.forFeature([DailyAwardsReceiving]),
  ],
  controllers: [DailyAwardsReceivingController],
  providers: [DailyAwardsReceivingService],
})
export class DailyAwardsReceivingModule {}
