import { RequestStatus } from '@relax/common/consts/RequestStatus';
import { BaseEntity } from '@relax/common/entities/BaseEntity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { DailyAwardsTable } from '../../daily-awards-tables/entities/daily-awards-table.entity';

@Entity()
export class DailyAwardsReceiving extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  uid: string;

  @Column({ type: 'timestamptz', default: () => 'NOW()' })
  recievedAt: Date;

  @Column()
  day: number;

  @Column({ enum: RequestStatus, default: RequestStatus.IDLE })
  requestStatus: RequestStatus;

  @ManyToOne(() => DailyAwardsTable, (table) => table.receivings)
  table: DailyAwardsTable;
}
