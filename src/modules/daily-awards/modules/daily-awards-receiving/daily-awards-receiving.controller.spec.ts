import { Test, TestingModule } from '@nestjs/testing';
import { DailyAwardsReceivingController } from './daily-awards-receiving.controller';
import { DailyAwardsReceivingService } from './daily-awards-receiving.service';

describe('DailyAwardsReceivingController', () => {
  let controller: DailyAwardsReceivingController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DailyAwardsReceivingController],
      providers: [DailyAwardsReceivingService],
    }).compile();

    controller = module.get<DailyAwardsReceivingController>(
      DailyAwardsReceivingController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
