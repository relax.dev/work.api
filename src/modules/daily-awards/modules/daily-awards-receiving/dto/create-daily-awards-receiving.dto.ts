import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateDailyAwardsReceivingDto {
  @IsNotEmpty()
  @ApiProperty()
  tableId: string;

  @IsNotEmpty()
  @ApiProperty()
  day: number;
}
