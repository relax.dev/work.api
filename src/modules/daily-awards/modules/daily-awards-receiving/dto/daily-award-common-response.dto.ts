import { ApiResponseProperty } from '@nestjs/swagger';
import { DailyAwardsTableItemDto } from '../../daily-awards-tables/dto/daily-awards-table-item.dto';

export class DailyAwardCommonResponseDto {
  @ApiResponseProperty()
  nextDay: number;

  @ApiResponseProperty()
  lastRecieveDay: number;

  @ApiResponseProperty()
  willResetAt: number;

  @ApiResponseProperty()
  willAvaliableAt: number;

  @ApiResponseProperty({ type: [DailyAwardsTableItemDto] })
  data: DailyAwardsTableItemDto[];
}
