import { DailyAwardsTableDto } from '../../daily-awards-tables/dto/daily-awards-table.dto';

export class DailyAwardsReceivingDto {
  id: string;
  table: DailyAwardsTableDto;
  day: number;
  recievedAt: Date;
}
