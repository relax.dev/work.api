import { Test, TestingModule } from '@nestjs/testing';
import { DailyAwardsReceivingService } from './daily-awards-receiving.service';

describe('DailyAwardsReceivingService', () => {
  let service: DailyAwardsReceivingService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DailyAwardsReceivingService],
    }).compile();

    service = module.get<DailyAwardsReceivingService>(
      DailyAwardsReceivingService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
