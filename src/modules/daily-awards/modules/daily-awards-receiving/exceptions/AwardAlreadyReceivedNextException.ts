import { HttpException, HttpStatus } from '@nestjs/common';

export const AwardAlreadyReceivedNextException = new HttpException(
  'Next award already received',
  HttpStatus.BAD_REQUEST,
);
