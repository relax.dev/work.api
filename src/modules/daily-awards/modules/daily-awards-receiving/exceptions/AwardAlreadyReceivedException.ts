import { HttpException, HttpStatus } from '@nestjs/common';

export const AwardAlreadyReceivedException = new HttpException(
  'Award already received',
  HttpStatus.BAD_REQUEST,
);
