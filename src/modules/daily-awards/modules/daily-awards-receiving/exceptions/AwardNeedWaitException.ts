import { HttpException, HttpStatus } from '@nestjs/common';

export const AwardNeedWaitException = new HttpException(
  'Need wait for receiving award',
  HttpStatus.BAD_REQUEST,
);
