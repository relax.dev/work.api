import { HttpException, HttpStatus } from '@nestjs/common';

export const AwardNeedReceivePreviousException = new HttpException(
  'Need recieve previous award',
  HttpStatus.BAD_REQUEST,
);
