import { HttpException, HttpStatus } from '@nestjs/common';

export const AwardDayIncorrectException = new HttpException(
  'Day is incorrect',
  HttpStatus.BAD_REQUEST,
);
