import { Module } from '@nestjs/common';
import { DailyAwardsReceivingModule } from './modules/daily-awards-receiving/daily-awards-receiving.module';
import { DailyAwardsTablesModule } from './modules/daily-awards-tables/daily-awards-tables.module';

@Module({
  imports: [DailyAwardsReceivingModule, DailyAwardsTablesModule],
})
export class DailyAwardsModule {}
