import {
  Controller,
  Get,
  Patch,
  UseGuards,
  UseInterceptors,
  UploadedFile,
  HttpException,
  HttpStatus,
  Query,
} from '@nestjs/common';
import { ProfilesService } from './profiles.service';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { JwtAuthGuard } from '@relax/modules/auth/guards/jwt-auth.guard';
import { CurrentUser } from '@relax/common/decorators/CurrentUser.decorator';
import { User } from '@relax/modules/users/entities/user.entity';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { ProfileDto } from '@relax/modules/profiles/dto/profile.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { ImagesService } from '@relax/modules/images/images.service';

@Controller('profiles')
@ApiTags('profiles')
export class ProfilesController {
  constructor(
    private readonly profilesService: ProfilesService,
    private readonly imagesService: ImagesService,
  ) {}

  @Get('/me')
  @UseGuards(JwtAuthGuard)
  @ApiResponse({ type: ProfileDto })
  getProfile(@CurrentUser() user: User): Promise<ProfileDto> {
    return this.profilesService.findByUserId(user.id);
  }

  @Patch('/me')
  @UseGuards(JwtAuthGuard)
  async updateProfile(
    @CurrentUser() user: User,
    @Query() payload: UpdateProfileDto,
  ) {
    const profile = await this.profilesService.findByUserId(user.id);

    if (!profile) {
      throw new HttpException('No profile', HttpStatus.BAD_REQUEST);
    }

    return this.profilesService.update(profile.id, payload);
  }

  @Patch('/me/avatar')
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(FileInterceptor('file'))
  async updateProfileAvatar(
    @CurrentUser() user: User,
    @UploadedFile('file') file: Express.Multer.File,
  ) {
    const sizes = [
      {
        width: 100,
      },
      {
        width: 500,
      },
    ];

    const profile = await this.profilesService.findByUserId(user.id);
    const image = await this.imagesService.upload({
      userId: user.id,
      file,
      sizes,
    });

    return this.profilesService.updateAvatar(profile.id, image.id);
  }
}
