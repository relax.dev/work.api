import { Module } from '@nestjs/common';
import { ProfilesService } from './profiles.service';
import { ProfilesController } from './profiles.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Profile } from '@relax/modules/profiles/entities/profile.entity';
import { ImagesModule } from '@relax/modules/images/images.module';

@Module({
  imports: [TypeOrmModule.forFeature([Profile]), ImagesModule],
  controllers: [ProfilesController],
  providers: [ProfilesService],
  exports: [ProfilesService],
})
export class ProfilesModule {}
