import { ApiProperty } from '@nestjs/swagger';
import { UserDto } from '@relax/modules/users/dto/user.dto';
import { ImageDto } from '@relax/modules/images/dto/image.dto';

export class ProfileDto {
  @ApiProperty()
  id: string;
  @ApiProperty()
  surName: string;
  @ApiProperty()
  firstName: string;
  @ApiProperty()
  lastName: string;
  @ApiProperty()
  user: UserDto;
  @ApiProperty()
  avatar: ImageDto;
}
