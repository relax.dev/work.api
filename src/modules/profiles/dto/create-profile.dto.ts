import { ApiProperty } from '@nestjs/swagger';

export class CreateProfileDto {
  @ApiProperty()
  firstName: string;

  @ApiProperty()
  lastName: string;

  @ApiProperty()
  surName?: string;

  @ApiProperty()
  bio?: string;

  @ApiProperty()
  shortName?: string;

  userId: string;
}
