import { Injectable } from '@nestjs/common';
import { CreateProfileDto } from './dto/create-profile.dto';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { Repository } from 'typeorm';
import { Profile } from '@relax/modules/profiles/entities/profile.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class ProfilesService {
  constructor(
    @InjectRepository(Profile)
    private readonly profilesRepository: Repository<Profile>,
  ) {}

  create(createProfileDto: CreateProfileDto) {
    const data = this.profilesRepository.create({
      ...createProfileDto,
      user: {
        id: createProfileDto.userId,
      },
    });

    return this.profilesRepository.save(data);
  }

  findAll() {
    return `This action returns all profiles`;
  }

  findByUserId(id: string) {
    return this.profilesRepository.findOne(
      { user: { id } },
      { relations: ['user', 'avatar'] },
    );
  }

  findByUserIds(ids: string[]) {
    return this.profilesRepository
      .createQueryBuilder('profile')
      .leftJoinAndSelect('profile.avatar', 'avatar')
      .leftJoinAndSelect('profile.user', 'user')
      .where('profile.user.id IN (:...ids)', { ids })
      .select([
        'profile.firstName',
        'profile.lastName',
        'profile.surName',
        'user.id',
        'avatar',
      ])
      .getMany();
  }

  findOne(id: string) {
    return `This action returns a #${id} profile`;
  }

  update(id: string, updateProfileDto: UpdateProfileDto) {
    return this.profilesRepository.update(id, updateProfileDto);
  }

  updateAvatar(id: string, avatarId: string) {
    return this.profilesRepository.update(id, {
      avatar: {
        id: avatarId,
      },
    });
  }

  remove(id: string) {
    return `This action removes a #${id} profile`;
  }
}
