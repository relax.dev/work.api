import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from '@relax/modules/users/entities/user.entity';
import { Image } from '@relax/modules/images/entities/image.entity';
import { BaseEntity } from '@relax/common/entities/BaseEntity';

@Entity()
export class Profile extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @OneToOne(() => User, (user) => user.profile)
  @JoinColumn()
  user: User;

  @Column({ type: 'varchar', nullable: true })
  firstName: string;

  @Column({ type: 'varchar', nullable: true })
  lastName: string;

  @Column({ type: 'varchar', nullable: true })
  surName: string;

  @Column({ type: 'varchar', nullable: true, unique: true })
  shortName: string;

  @Column({ type: 'varchar', nullable: true })
  bio: string;

  @JoinColumn()
  @OneToOne(() => Image, { eager: true })
  avatar: Image;
}
