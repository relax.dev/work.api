import { Module } from '@nestjs/common';
import { VerificationsController } from './verifications.controller';
import { VerificationsService } from './verifications.service';
import { VerificationsListener } from '@relax/modules/verifications/listeners/verifications.listener';

@Module({
  imports: [],
  controllers: [VerificationsController],
  providers: [VerificationsService, VerificationsListener],
  exports: [VerificationsService],
})
export class VerificationsModule {}
