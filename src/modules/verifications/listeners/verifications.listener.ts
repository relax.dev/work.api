import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { VerificationsService } from '@relax/modules/verifications/verifications.service';
import { VerificationConfirmEmailSendEvent } from '@relax/common/events/payloads/verification-confirm-email-send.event';
import { CommonEventType } from '@relax/common/events/types/common';

@Injectable()
export class VerificationsListener {
  constructor(private readonly verificationsService: VerificationsService) {}
  @OnEvent(CommonEventType.VerificationsConfirmEmailSend)
  handleOrderCreatedEvent(event: VerificationConfirmEmailSendEvent) {
    this.verificationsService.sendVerifyEmail(event.user).catch(() => {
      console.error('Can not send email');
    });
  }
}
