import {
  Controller,
  Get,
  Post,
  Response,
  Query,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '@relax/modules/auth/guards/jwt-auth.guard';
import { CurrentUser } from '@relax/common/decorators/CurrentUser.decorator';
import { SignedUrlGuard } from 'nestjs-url-generator';
import { VerificationsService } from '@relax/modules/verifications/verifications.service';
import { Throttle } from '@nestjs/throttler';
import { ConfigService } from '@nestjs/config';
import { ApplicationConfig } from '@relax/configuration/config';
import { ApiConflictResponse, ApiResponse, ApiTags } from '@nestjs/swagger';
import { makeRedirectResponse } from '@relax/common/helpers/makeRedirectResponse';
import { User } from '@relax/modules/users/entities/user.entity';
@Controller('verifications')
@ApiTags('verifications')
export class VerificationsController {
  constructor(
    private readonly configService: ConfigService<ApplicationConfig>,
    private readonly verificationsService: VerificationsService,
  ) {}

  @Get('email/verify')
  @UseGuards(SignedUrlGuard)
  @ApiConflictResponse({
    description:
      'Редирект на failureUrl с query параметрами statusCode и errorMessage ',
  })
  @ApiResponse({
    status: 403,
    description: 'Редирект на successUrl',
  })
  async verifyEmail(@Query('userId') userId, @Response() response) {
    try {
      this.verificationsService.processVerifyEmail(userId);
      return makeRedirectResponse(response, this.configService, {
        configPath: 'emailConfirmation',
      });
    } catch (error) {
      return makeRedirectResponse(response, this.configService, {
        configPath: 'emailConfirmation',
        error,
      });
    }
  }

  @Post('email/verify/send')
  @UseGuards(JwtAuthGuard)
  @Throttle(1, 60)
  sendVerifyEmail(@CurrentUser() user: User) {
    return this.verificationsService.sendVerifyEmail(user);
  }
}
