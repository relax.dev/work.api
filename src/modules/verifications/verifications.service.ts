import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UrlGeneratorService } from 'nestjs-url-generator';
import * as dayjs from 'dayjs';
import { VerificationsController } from '@relax/modules/verifications/verifications.controller';
import { MailerService } from '@nestjs-modules/mailer';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { VerificationConfirmEmailEvent } from '@relax/common/events/payloads/verification-confirm-email.event';
import { CommonEventType } from '@relax/common/events/types/common';
import { User } from '@relax/modules/users/entities/user.entity';

@Injectable()
export class VerificationsService {
  constructor(
    private readonly urlGeneratorService: UrlGeneratorService,
    private readonly mailerService: MailerService,
    private readonly eventEmitter: EventEmitter2,
  ) {}

  getVerifyEmailLink(user: User) {
    return this.urlGeneratorService.signControllerUrl({
      controller: VerificationsController,
      controllerMethod: VerificationsController.prototype.verifyEmail,
      expirationDate: dayjs().add(180, 'days').toDate(),
      query: {
        userId: user.id,
      },
    });
  }

  sendVerifyEmail(user: User) {
    const link = this.getVerifyEmailLink(user);

    try {
      return this.mailerService.sendMail({
        to: user.email, // list of receivers,
        template: './reset-password.template.hbs',
        context: {
          url: link,
        },
        subject: 'Подтверждение электронной почты на Relax.Work', // Subject line
      });
    } catch (e) {
      throw new HttpException(
        'Can not send verification email',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  processVerifyEmail(userId: string) {
    const event = new VerificationConfirmEmailEvent();

    event.userId = userId;

    return this.eventEmitter.emit(
      CommonEventType.VerificationsConfirmEmail,
      event,
    );
  }
}
