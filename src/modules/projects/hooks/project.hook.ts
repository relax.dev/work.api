import { ExecutionContext, Injectable } from '@nestjs/common';
import { IProjectHook } from '../decorators/RequiredProjectHook';

@Injectable()
export class ProjectHook implements IProjectHook {
  async getProjectId(ctx: ExecutionContext) {
    const request = ctx.switchToHttp().getRequest();

    return (
      request.params.projectId ||
      request.body.projectId ||
      request.query.projectId
    );
  }
}
