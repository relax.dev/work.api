import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmpty, ValidateNested } from 'class-validator';
import { ProjectParticipantDto } from './project-participant.dto';

export class AddParticipantsProjectDto {
  @IsNotEmpty()
  @ApiProperty({ type: [ProjectParticipantDto] })
  @ValidateNested()
  @Type(() => ProjectParticipantDto)
  participants: ProjectParticipantDto[];
}
