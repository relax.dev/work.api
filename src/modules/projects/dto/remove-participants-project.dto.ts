import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class RemoveParticipantsProjectDto {
  @IsNotEmpty()
  @ApiProperty()
  ids: string[];
}
