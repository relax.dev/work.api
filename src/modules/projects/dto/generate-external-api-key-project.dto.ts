import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class GenerateExternalApiKeyDto {
  @ApiProperty()
  @IsNotEmpty()
  uid: string;

  @ApiProperty()
  @IsNotEmpty()
  projectId: string;

  @ApiProperty()
  @IsNotEmpty()
  secretKey: string;
}
