import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';
import { ProjectRoles } from '../consts/roles';

export class ProjectParticipantDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  userId: string;

  @ApiProperty({ enum: ProjectRoles })
  @IsNotEmpty()
  role: ProjectRoles;
}
