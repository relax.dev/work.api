import { ApiResponseProperty } from '@nestjs/swagger';
import { ProjectParticipantDto } from './project-participant.dto';

export class AddParticipantsProjectResponseDto {
  @ApiResponseProperty({ type: [ProjectParticipantDto] })
  added: ProjectParticipantDto[];
  @ApiResponseProperty({ type: [ProjectParticipantDto] })
  existence: ProjectParticipantDto[];
  @ApiResponseProperty({ type: [ProjectParticipantDto] })
  invalid: ProjectParticipantDto[];
}
