import { ApiProperty } from '@nestjs/swagger';
import { ProjectParticipantDto } from './project-participant.dto';

export class ProjectDto {
  @ApiProperty()
  id: string;

  @ApiProperty()
  participants: ProjectParticipantDto[];

  @ApiProperty()
  secretKey: string;
}
