import { Module } from '@nestjs/common';
import { ProjectsService } from './projects.service';
import { ProjectsController } from './projects.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Project } from './entities/project.entity';
import { UsersModule } from '../users/users.module';
import { ProfilesModule } from '../profiles/profiles.module';
import { ProjectParticipant } from './entities/project-participant.entity';
import { ProjectHook } from './hooks/project.hook';
import { ProjectUIDGuard } from './guards/project-uid.guard';

@Module({
  imports: [
    TypeOrmModule.forFeature([Project, ProjectParticipant]),
    UsersModule,
    ProfilesModule,
  ],
  controllers: [ProjectsController],
  providers: [ProjectsService, ProjectHook, ProjectUIDGuard],
  exports: [ProjectsService, ProjectUIDGuard],
})
export class ProjectsModule {}
