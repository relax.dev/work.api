import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { RelaxCrypto } from '@relax/common/utils/RelaxCrypto';
import { ProjectsService } from '../projects.service';

@Injectable()
export class ProjectUIDGuard implements CanActivate {
  constructor(private readonly projectsService: ProjectsService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const token = request.headers['project-token'];

    console.log('token', token);

    if (!token) return false;

    const tokenParts = token.split('.');

    try {
      const payload = JSON.parse(
        Buffer.from(tokenParts[1], 'base64').toString('utf8'),
      );

      console.log('payload', payload);

      const project = await this.projectsService.findOne(payload.projectId);

      const RC = new RelaxCrypto(project.secretKey);

      const data = RC.decrypt(token);

      if (!data) {
        return false;
      } else {
        if (!request.external) {
          request.external = { uid: data.uid };
        } else {
          request.external.uid = data.uid;
        }

        return true;
      }
    } catch (err) {
      return false;
    }
  }
}
