import { applyDecorators, UseGuards } from '@nestjs/common';
import { AnyClass } from '@relax/common/types/AnyClass';
import { JwtAuthGuard } from '@relax/modules/auth/guards/jwt-auth.guard';
import { ProjectRoles } from '../consts/roles';
import {
  IProjectHook,
  RequiredProjectHook,
} from '../decorators/RequiredProjectHook';
import { RequiredProjectRoles } from '../decorators/RequiredProjectRoles';
import { ProjectRolesGuard } from './project-roles.guard';

export const ProjectRolesComplexGuard = ({
  roles,
  hook,
}: {
  roles: ProjectRoles[];
  hook: AnyClass<IProjectHook>;
}) =>
  applyDecorators(
    RequiredProjectRoles(...roles),
    RequiredProjectHook(hook),
    UseGuards(JwtAuthGuard),
    UseGuards(ProjectRolesGuard),
  );
