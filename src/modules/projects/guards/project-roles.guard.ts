import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ModuleRef, Reflector } from '@nestjs/core';
import { AnyClass } from '@relax/common/types/AnyClass';
import { ProjectRoles } from '../consts/roles';
import { IProjectHook } from '../decorators/RequiredProjectHook';
import { ProjectsService } from '../projects.service';

@Injectable()
export class ProjectRolesGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly moduleRef: ModuleRef,
    private readonly projectsService: ProjectsService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const Hook = this.reflector.get<AnyClass<IProjectHook>>(
      'projectHook',
      context.getHandler(),
    );
    const roles = this.reflector.get<ProjectRoles[]>(
      'projectRoles',
      context.getHandler(),
    );

    const { user } = context.switchToHttp().getRequest();

    const hook = await this.moduleRef.create<IProjectHook>(Hook);

    const projectId = await hook.getProjectId(context);

    if (!projectId) {
      return null;
    }

    const participant = await this.projectsService.getParticipant(
      projectId,
      user.id,
    );

    const participantRole = participant?.role ?? null;

    if (participantRole === ProjectRoles.OWNER) return true;

    if (roles.includes(participantRole)) {
      return true;
    }

    return false;
  }
}
