import { BaseEntity } from '@relax/common/entities/BaseEntity';
import { User } from '@relax/modules/users/entities/user.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ProjectRoles } from '../consts/roles';
import { Project } from './project.entity';

@Entity()
export class ProjectParticipant extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => Project, (project) => project.participants, {
    cascade: true,
  })
  project: Project;

  @Column({ enum: ProjectRoles })
  role: ProjectRoles;

  @ManyToOne(() => User, { cascade: true, eager: true })
  @JoinColumn()
  user: User;
}
