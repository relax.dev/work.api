import { BaseEntity } from '@relax/common/entities/BaseEntity';
import { DailyAwardsTable } from '@relax/modules/daily-awards/modules/daily-awards-tables/entities/daily-awards-table.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ProjectParticipant } from './project-participant.entity';

@Entity()
export class Project extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: true })
  secretKey: string;

  @OneToMany(() => ProjectParticipant, (participant) => participant.project)
  participants: ProjectParticipant[];

  @OneToMany(() => DailyAwardsTable, (table) => table.project)
  dailyAwardsTables: DailyAwardsTable[];
}
