import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { ProjectsService } from './projects.service';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { CurrentUser } from '@relax/common/decorators/CurrentUser.decorator';
import { UserDto } from '../users/dto/user.dto';
import { AddParticipantsProjectDto } from './dto/add-participants-project.dto';
import { ProjectRoles } from './consts/roles';
import { ApiTags } from '@nestjs/swagger';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import { RemoveParticipantsProjectDto } from './dto/remove-participants-project.dto';
import { ProfilesService } from '../profiles/profiles.service';
import { ProjectHook } from './hooks/project.hook';
import { ProjectRolesComplexGuard } from './guards/project-roles-complex.guard';
import { GenerateExternalApiKeyDto } from './dto/generate-external-api-key-project.dto';

@Controller('projects')
@ApiTags('projects')
export class ProjectsController {
  constructor(
    private readonly projectsService: ProjectsService,
    private readonly profilesService: ProfilesService,
  ) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  create(
    @Body() createProjectDto: CreateProjectDto,
    @CurrentUser() user: UserDto,
  ) {
    return this.projectsService.create(user.id, createProjectDto);
  }

  @Post('/token')
  generateExternalApiKey(@Body() dto: GenerateExternalApiKeyDto) {
    return this.projectsService.generateExternalApiKey(dto);
  }

  @Post(':projectId/participants')
  @ProjectRolesComplexGuard({
    roles: [ProjectRoles.ADMIN],
    hook: ProjectHook,
  })
  addParticipants(
    @Param('projectId') id: string,
    @Body() addParticipantsDto: AddParticipantsProjectDto,
  ) {
    return this.projectsService.addParticipants(id, addParticipantsDto);
  }

  @Post(':projectId/secretKey')
  @ProjectRolesComplexGuard({
    roles: [ProjectRoles.ADMIN, ProjectRoles.DEVELOPER],
    hook: ProjectHook,
  })
  changeSecretKey(@Param('projectId') id: string) {
    return this.projectsService.changeSecretKey(id);
  }

  @Delete(':projectId/participants')
  @ProjectRolesComplexGuard({
    roles: [ProjectRoles.ADMIN],
    hook: ProjectHook,
  })
  removeParticipants(
    @Param('projectId') id: string,
    @Body() removeParticipantsDto: RemoveParticipantsProjectDto,
  ) {
    return this.projectsService.removeParticipants(id, removeParticipantsDto);
  }

  @Get(':projectId')
  @ProjectRolesComplexGuard({
    roles: [
      ProjectRoles.ADMIN,
      ProjectRoles.DEVELOPER,
      ProjectRoles.EDITOR,
      ProjectRoles.OWNER,
    ],
    hook: ProjectHook,
  })
  async findOne(@Param('projectId') id: string) {
    return this.projectsService.findOne(id);
  }

  @Get()
  @UseGuards(JwtAuthGuard)
  findUserProjects(
    @CurrentUser() user: UserDto,
    @Paginate() query: PaginateQuery,
  ) {
    return this.projectsService.findWhereUserIsParticipant(user.id, query);
  }

  @Patch(':projectId')
  @ProjectRolesComplexGuard({
    roles: [ProjectRoles.ADMIN],
    hook: ProjectHook,
  })
  update(
    @Param('projectId') id: string,
    @Body() updateProjectDto: UpdateProjectDto,
  ) {
    return this.projectsService.update(id, updateProjectDto);
  }

  @Delete(':projectId')
  @ProjectRolesComplexGuard({
    roles: [ProjectRoles.OWNER],
    hook: ProjectHook,
  })
  remove(@Param('projectId') id: string) {
    return this.projectsService.remove(id);
  }
}
