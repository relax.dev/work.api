import { SetMetadata } from '@nestjs/common';
import { ProjectRoles } from '../consts/roles';

export const RequiredProjectRoles = (...roles: ProjectRoles[]) =>
  SetMetadata('projectRoles', roles);
