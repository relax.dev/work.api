import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const ProjectUID = createParamDecorator(
  (data: string, ctx: ExecutionContext): string | null => {
    const uid: string = ctx.switchToHttp().getRequest().external?.uid;

    if (!uid) {
      return null;
    }

    return uid;
  },
);
