import { ExecutionContext, SetMetadata } from '@nestjs/common';

export interface IProjectHook {
  getProjectId(ctx: ExecutionContext): string | null | Promise<string | null>;
}

export const RequiredProjectHook = <T>(hook: T) =>
  SetMetadata('projectHook', hook);
