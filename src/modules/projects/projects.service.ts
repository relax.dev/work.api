import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { paginate, PaginateQuery } from 'nestjs-paginate';
import { Repository } from 'typeorm';
import { UsersService } from '../users/users.service';
import { ProjectRoles } from './consts/roles';
import { AddParticipantsProjectDto } from './dto/add-participants-project.dto';
import { CreateProjectDto } from './dto/create-project.dto';
import { RemoveParticipantsProjectDto } from './dto/remove-participants-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { Project } from './entities/project.entity';
import { generateApiKey } from 'generate-api-key';
import { ProjectParticipant } from './entities/project-participant.entity';
import { ProjectParticipantDto } from './dto/project-participant.dto';
import { ProjectUserAlreadyParticipantException } from './exceptions/ProjectUserAlreadyParticipantException';
import { GenerateExternalApiKeyDto } from './dto/generate-external-api-key-project.dto';
import { RelaxCrypto } from '@relax/common/utils/RelaxCrypto';

@Injectable()
export class ProjectsService {
  constructor(
    @InjectRepository(Project)
    private readonly projectsRepository: Repository<Project>,
    @InjectRepository(ProjectParticipant)
    private readonly projectsParticipantsRepository: Repository<ProjectParticipant>,
    private readonly usersService: UsersService,
  ) {}

  async generateExternalApiKey(dto: GenerateExternalApiKeyDto) {
    const { secretKey, uid, projectId } = dto;

    const RC = new RelaxCrypto(secretKey);

    return { token: RC.encrypt({ uid, projectId }) };
  }

  async create(userId: string, payload: CreateProjectDto) {
    const data = this.projectsRepository.create({
      ...payload,
      secretKey: this.getNextSecretKey(),
    });

    const project = await this.projectsRepository.save(data);

    await this.addParticipants(project.id, {
      participants: [
        {
          userId,
          role: ProjectRoles.OWNER,
        },
      ],
    });

    return project;
  }

  getQueryBuilder() {
    return this.projectsRepository
      .createQueryBuilder('project')
      .leftJoinAndSelect('project.participants', 'participant')
      .leftJoinAndSelect('participant.user', 'user')
      .leftJoinAndSelect('user.profile', 'profile')
      .leftJoinAndSelect('profile.avatar', 'avatar')
      .select([
        'project',
        'participant',
        'user.id',
        'profile.firstName',
        'profile.lastName',
        'profile.id',
        'avatar',
      ]);
  }

  findWhereUserIsParticipant(userId: string, query: PaginateQuery) {
    const queryBuilder = this.getQueryBuilder().where('user.id = :userId', {
      userId,
    });

    return paginate<Project>(query, queryBuilder, {
      sortableColumns: ['id'],
      searchableColumns: ['id'],
      defaultSortBy: [['id', 'DESC']],
    });
  }

  async addParticipants(id: string, payload: AddParticipantsProjectDto) {
    const promises = [];
    for (const p of payload.participants) {
      promises.push(this.addParticipant(id, p));
    }

    return await Promise.all(promises);
  }

  async addParticipant(id: string, payload: ProjectParticipantDto) {
    const participant = await this.projectsParticipantsRepository.findOne({
      project: { id },
      user: { id: payload.userId },
    });

    if (participant) {
      throw ProjectUserAlreadyParticipantException;
    }

    const data = this.projectsParticipantsRepository.create({
      role: payload.role,
      user: {
        id: payload.userId,
      },
      project: {
        id,
      },
    });

    return this.projectsParticipantsRepository.save(data);
  }

  getParticipant(projectId: string, userId: string) {
    return this.projectsParticipantsRepository.findOne({
      project: { id: projectId },
      user: { id: userId },
    });
  }

  async removeParticipants(id: string, payload: RemoveParticipantsProjectDto) {
    const entities = await this.projectsParticipantsRepository.findByIds(
      payload.ids,
    );

    if (!entities) {
      throw new NotFoundException(
        `Some Entities not found, no changes applied!`,
      );
    }

    return this.projectsParticipantsRepository.remove(entities);
  }

  findAll() {
    // return this.projectsRepository.findOne(id)
  }

  getNextSecretKey() {
    return generateApiKey({ method: 'string' }).toString();
  }

  async changeSecretKey(id: string) {
    const key = this.getNextSecretKey();

    await this.update(id, {
      secretKey: key,
    });

    return {
      secretKey: key,
    };
  }

  findOne(id: string) {
    return this.getQueryBuilder()
      .where('project.id = :id', {
        id,
      })
      .getOne();
  }

  update(id: string, updateProjectDto: UpdateProjectDto) {
    return this.projectsRepository.update(id, updateProjectDto);
  }

  remove(id: string) {
    return this.projectsRepository.softDelete(id);
  }
}
