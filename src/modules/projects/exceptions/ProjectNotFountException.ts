import { HttpException, HttpStatus } from '@nestjs/common';

export const ProjectNotFountException = new HttpException(
  'Project not found',
  HttpStatus.BAD_REQUEST,
);
