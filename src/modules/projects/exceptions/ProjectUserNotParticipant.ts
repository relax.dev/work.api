import { HttpException, HttpStatus } from '@nestjs/common';

export const ProjectUserNotParticipantException = new HttpException(
  'User not participant in this project',
  HttpStatus.BAD_REQUEST,
);
