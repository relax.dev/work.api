import { HttpException, HttpStatus } from '@nestjs/common';

export const ProjectUserAlreadyParticipantException = new HttpException(
  'User already participant',
  HttpStatus.BAD_REQUEST,
);
