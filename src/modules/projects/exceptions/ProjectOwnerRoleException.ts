import { HttpException, HttpStatus } from '@nestjs/common';

export const ProjectOwnerRoleException = new HttpException(
  'Can not add OWNER role to other user',
  HttpStatus.BAD_REQUEST,
);
