export enum ProjectRoles {
  OWNER = 0,
  ADMIN = 1,
  DEVELOPER = 2,
  EDITOR = 3,
}
