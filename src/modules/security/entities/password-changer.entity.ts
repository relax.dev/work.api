import { BaseEntity } from '@relax/common/entities/BaseEntity';
import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class PasswordChanger extends BaseEntity {
  @PrimaryColumn({ type: 'varchar', unique: true })
  email: string;

  @Column({ type: 'varchar', nullable: true })
  hash: string;
}
