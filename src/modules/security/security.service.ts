import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UsersService } from '@relax/modules/users/users.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PasswordChanger } from '@relax/modules/security/entities/password-changer.entity';
import {
  ChangePasswordByHashDto,
  ResetPasswordDto,
} from '@relax/modules/security/dto/reset-password.dto';
import * as bcrypt from 'bcrypt';
import * as dayjs from 'dayjs';
import { MailerService } from '@nestjs-modules/mailer';
import { UrlGeneratorService } from 'nestjs-url-generator';
import { SecurityController } from '@relax/modules/security/security.controller';

@Injectable()
export class SecurityService {
  constructor(
    private readonly usersService: UsersService,
    private readonly urlGeneratorService: UrlGeneratorService,
    private readonly mailerService: MailerService,
    @InjectRepository(PasswordChanger)
    private readonly passwordChangersRepository: Repository<PasswordChanger>,
  ) {}

  async resetPassword(payload: ResetPasswordDto) {
    const hash = bcrypt.hashSync(Date.now().toString(), bcrypt.genSaltSync());

    const user = await this.usersService.findByEmail(payload.email);

    if (!user) {
      throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
    }

    await this.passwordChangersRepository.upsert(
      {
        email: payload.email,
        hash,
      },
      ['email'],
    );

    const link = this.getResetPasswordLink(payload.email, hash);

    return this.mailerService.sendMail({
      to: payload.email, // list of receivers,
      subject: 'Сброс пароля для сервиса Relax.Work', // Subject line
      html: `Что бы сбросить пароль - перейдите по ссылке: <link href="${link}">${link}</link>`, // HTML body content
    });
  }

  getResetPasswordLink(email: string, hash: string) {
    return this.urlGeneratorService.signControllerUrl({
      controller: SecurityController,
      controllerMethod: SecurityController.prototype.resetPasswordCallback,
      expirationDate: dayjs().add(180, 'days').toDate(),
      query: {
        hash,
      },
    });
  }

  async changePasswordByHash(payload: ChangePasswordByHashDto) {
    const changer = await this.passwordChangersRepository.findOne({
      hash: payload.hash,
    });

    const hashedPassword = bcrypt.hashSync(
      payload.nextPassword,
      bcrypt.genSaltSync(),
    );

    if (!changer?.hash) {
      throw new HttpException(
        'Password change process not initiated',
        HttpStatus.BAD_REQUEST,
      );
    }

    if (changer.hash !== payload.hash) {
      throw new HttpException('Incorrect hash', HttpStatus.BAD_REQUEST);
    }

    const user = await this.usersService.findByEmail(changer.email);

    if (!user) {
      throw new HttpException('Invalid email', HttpStatus.BAD_REQUEST);
    }

    if (bcrypt.compareSync(payload.nextPassword, user.password)) {
      throw new HttpException('Password already used', HttpStatus.BAD_REQUEST);
    }

    await this.usersService.update(user.id, {
      password: hashedPassword,
    });

    await this.passwordChangersRepository.update(
      { email: changer.email },
      {
        hash: null,
      },
    );

    return true;
  }
}
