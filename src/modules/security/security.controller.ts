import {
  Body,
  Controller,
  Get,
  Response,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { SecurityService } from '@relax/modules/security/security.service';
import {
  ChangePasswordByHashDto,
  ResetPasswordDto,
} from '@relax/modules/security/dto/reset-password.dto';
import { SignedUrlGuard } from 'nestjs-url-generator';
import { ApiTags } from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';
import { ApplicationConfig } from '@relax/configuration/config';
import { Throttle } from '@nestjs/throttler';
import { makeRedirectResponse } from '@relax/common/helpers/makeRedirectResponse';

@Controller('security')
@ApiTags('security')
export class SecurityController {
  constructor(
    private readonly securityService: SecurityService,
    private readonly configService: ConfigService<ApplicationConfig>,
  ) {}

  @Post('password/reset')
  @Throttle(1, 10)
  resetPassword(@Body() payload: ResetPasswordDto) {
    return this.securityService.resetPassword(payload);
  }

  @Post('password/change')
  async changePasswordByHash(@Body() payload: ChangePasswordByHashDto) {
    return this.securityService.changePasswordByHash(payload);
  }

  @Get('password/reset/callback')
  @UseGuards(SignedUrlGuard)
  async resetPasswordCallback(
    @Query('hash') hash: string,
    @Response() response,
  ) {
    return makeRedirectResponse(response, this.configService, {
      configPath: 'resetPassword',
      query: { hash },
    });
  }
}
