import { Module } from '@nestjs/common';
import { SecurityController } from './security.controller';
import { SecurityService } from './security.service';
import { UsersModule } from '@relax/modules/users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PasswordChanger } from '@relax/modules/security/entities/password-changer.entity';

@Module({
  imports: [TypeOrmModule.forFeature([PasswordChanger]), UsersModule],
  controllers: [SecurityController],
  providers: [SecurityService],
})
export class SecurityModule {}
