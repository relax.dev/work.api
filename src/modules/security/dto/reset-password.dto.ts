import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class ResetPasswordDto {
  @ApiProperty()
  @IsNotEmpty()
  email: string;
}

export class ChangePasswordDto extends ResetPasswordDto {
  @ApiProperty()
  @IsNotEmpty()
  nextPassword: string;
}

export class ChangePasswordByHashDto {
  @ApiProperty()
  @IsNotEmpty()
  nextPassword: string;

  @ApiProperty()
  @IsNotEmpty()
  hash: string;
}
