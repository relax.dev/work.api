import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ApplicationConfig, createConfig } from './configuration/config';
import { UsersModule } from '@relax/modules/users/users.module';
import { AuthModule } from '@relax/modules/auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { TokensModule } from '@relax/modules/tokens/tokens.module';
import { AccessControlModule } from 'nest-access-control';
import { roles } from '@relax/common/roles/app.roles';
import { UrlGeneratorModule } from 'nestjs-url-generator';
import { VerificationsModule } from '@relax/modules/verifications/verifications.module';
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler';
import { APP_GUARD } from '@nestjs/core';
import { SecurityModule } from '@relax/modules/security/security.module';
import { ImagesModule } from '@relax/modules/images/images.module';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { ProjectsModule } from './modules/projects/projects.module';
import { DailyAwardsModule } from './modules/daily-awards/daily-awards.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [createConfig],
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService<ApplicationConfig>) => {
        return config.get('typeorm');
      },
    }),
    MailerModule.forRoot({
      transport: {
        host: 'smtp-relay.sendinblue.com',
        secure: true,
        auth: {
          user: 'rnuiskov@gmail.com',
          pass: '6Eb1LJtX4FmkO2v9',
        },
      },
      defaults: {
        from: '"Relax.Work" <rnuiskov@gmail.com>',
      },
      template: {
        dir: __dirname + '/templates',
        adapter: new HandlebarsAdapter(),
        options: {
          strict: true,
        },
      },
    }),
    UrlGeneratorModule.forRootAsync({
      useFactory: (config: ConfigService<ApplicationConfig>) => ({
        secret: config.get('tokens.mailer.secretKey', { infer: true }),
        appUrl: config.get('application.url', { infer: true }),
      }),
      imports: [ConfigModule],
      inject: [ConfigService],
    }),
    ThrottlerModule.forRoot({
      ttl: 1,
      limit: 3,
    }),
    VerificationsModule,
    AuthModule,
    UsersModule,
    TokensModule,
    SecurityModule,
    ImagesModule,
    ProjectsModule,
    DailyAwardsModule,
    AccessControlModule.forRoles(roles),
    EventEmitterModule.forRoot(),
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard,
    },
  ],
})
export class AppModule {}
