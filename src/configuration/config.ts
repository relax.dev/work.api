import * as typeORMConfig from './ormconfig';

export const config = {
  application: {
    port: parseInt(process.env.PORT, 10) || 3000,
    url: 'http://localhost:3333',
  },
  typeorm: typeORMConfig,
  s3: {
    address: 'storage.yandexcloud.net',
    bucket: 'relax.work',
  },
  tokens: {
    access: {
      expiresIn: '30d',
      secretKey: 'asdasdg',
    },
    refresh: {
      expiresIn: '180d',
      secretKey: 'dfsfsd',
    },
    mailer: {
      secretKey: 'asdcdasd',
    },
  },
  redirects: {
    authorization: {
      successUrl: 'https://vk.com/web.rnskv',
      failureUrl: 'https://vk.com/id1',
    },
    resetPassword: {
      successUrl: 'http://localhost:3000/auth/reset-password',
      failureUrl: 'http://localhost:3000/auth/reset-password',
    },
    emailConfirmation: {
      successUrl: 'https://vk.com/web.rnskv',
      failureUrl: 'https://vk.com/id1',
    },
  },
  google: {
    clientId:
      '752693626233-aik6h6n5ipsctmjbepevsnkpq5n8brkm.apps.googleusercontent.com',
    clientSecret: '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',
    redirectUrl: 'http://localhost:3333/auth/google/redirect',
    scope: ['email', 'profile'],
  },
} as const;

export const createConfig = () => config;
export type ApplicationConfig = ReturnType<typeof createConfig>;
