import * as dotenv from 'dotenv';
import { join } from 'path';
import type { TypeOrmModuleOptions } from '@nestjs/typeorm';

const databasePath = '../database';
const modulesPath = '../modules';

dotenv.config();

const configs: TypeOrmModuleOptions & { seeds: string[]; factories: string[] } =
  {
    type: 'postgres',
    url: process.env.DATABASE_CONNECTION_URL,
    entities: [join(__dirname, modulesPath, '/**/*.entity{.ts,.js}')],
    migrations: [join(__dirname, databasePath, '/migrations/*{.ts,.js}')],
    cli: {
      migrationsDir: join(__dirname, databasePath, '/migrations'),
    },
    seeds: [join(__dirname, databasePath, '/seeds/**/*{.ts,.js}')],
    factories: [join(__dirname, databasePath, '/factories/**/*{.ts,.js}')],
    synchronize: false,
    autoLoadEntities: true,
  };

module.exports = configs;
