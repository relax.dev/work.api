import { NestFactory, Reflector } from '@nestjs/core';
import { AppModule } from './app.module';
import { connectSwagger } from './swagger/swagger.connector';
import { config as appConfig } from '@relax/configuration/config';
import {
  ClassSerializerInterceptor,
  Logger,
  ValidationPipe,
} from '@nestjs/common';
import { TypeOrmFilter } from '@relax/common/filters/typeorm.exception.filter';
const logger = new Logger('Core/App');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors({
    origin: '*',
  });

  app.useGlobalFilters(new TypeOrmFilter());

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
    }),
  );
  app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));

  connectSwagger(app);

  await app.listen(appConfig.application.port);
}

bootstrap()
  .then(() => {
    logger.verbose(`Server started at port ${appConfig.application.port}`);
  })
  .catch((e) => {
    logger.log('Server started failed with error', e);
  });
