import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { INestApplication } from '@nestjs/common';

export const connectSwagger = (app: INestApplication) => {
  const config = new DocumentBuilder()
    .setTitle('API to the moon!')
    .setDescription('API для relax.work')
    .setVersion('1.0')
    .addTag('relax.work')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('swagger', app, document);
};
